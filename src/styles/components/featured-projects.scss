.rp_featured-projects {
  @include container;


  &--hidden {
    pointer-events: none;

    & .rp_featured-projects__image,
    & .rp_featured-projects__title,
    & .rp_featured-projects__description {
      transition: none;
      opacity: 0;
      pointer-events: none;
    }
  }


  &__list {
    @include content;

    @include xl {
      width: span(8 of 12);
    }
  }


  &__item {
    position: relative;
    margin-top: 15px;
    &:not(:nth-child(n + 2)) {
      margin-top: 0;
    }

    @include sm {
      margin-top: 20px;
      &:not(:nth-child(n + 2)) {
        margin-top: 0;
      }
    }

    @include md {
      &:hover .rp_featured-projects__description,
      &:hover .rp_featured-projects__image {
        opacity: 1;
      }
      &:hover .rp_featured-projects__title {
        color: $white;
      }
    }
  }


  &__image {
    display: none;
    pointer-events: none;
    opacity: 0;

    @include md {
      display: block;
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      object-fit: cover;
      z-index: 2;
      transition: opacity .6s .21s;
    }
  }


  &__title {
    @include font(medium);
    @include type(md);
    cursor: pointer;
    display: block;
    color: $gray;
    transition: color .35s .21s ease-in-out,
                opacity .35s ease-in-out;
    opacity: 1;
    position: relative;
    z-index: 5;

    &:hover {
      color: $black;
    }
  }


  &__description {
    display: none;
    opacity: 0;

    @include md {
      @include font(medium);
      @include type(md);
      transition: opacity .35s .21s ease-in-out;
      display: block;
      z-index: 5;
      position: absolute;
      top: 0;
      right: 0;
      width: span(2 of 4);
      color: $white;
    }

    @include lg {
      width: span(4 of 10);
      left: span(4 of 10 wide);
      right: auto;
    }

    @include xl {
      width: span(4 of 8);
      left: span(4 of 8 wide);
      left: auto;
      right: 0;
    }
  }
}
