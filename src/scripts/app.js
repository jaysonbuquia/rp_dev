// Import packages
import $ from 'jquery';

// Import modules
import expandable from './components/expandable.js';
import teamList from './components/team-list.js';
import locationTabs from './components/location-tabs.js';
import modal from './components/modal.js';
import map from './components/map.js';


// Declare globals
window.$ = window.jQuery = $;


// Initialize modules
$(document).ready(() => {

  [

    // List down modules to initialize
    expandable,
    teamList,
    locationTabs,
    modal,
    map,

  ].map( module => {
    if (module && typeof module.init == 'function') {
      module.init();
    }
  });

});
