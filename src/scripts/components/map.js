import Component from '../modules/Component';

class Map extends Component {

  constructor($element) {
    super($element);

    this.lat = this.$element.data('lat');
    this.long = this.$element.data('long');
    this.pin = this.$element.data('pin');

    this.initMap();
  }


  initMap() {
    this.Map = new google.maps.Map(this.$element[0], {
                  "center": {lat: this.lat, lng: this.long},
                  "zoom": 17,
                  "scrollwheel": false,
                  "panControl": true,
                  "draggable": true,
                  "mapTypeControl": false,
                  "scaleControl": false,
                  "streetViewControl": false,
                  "overviewMapControl": false,
                  "styles": [
                    {
                      "featureType": "landscape",
                      "stylers": [
                        {
                          "color": "#F2F2F2"
                        }
                      ]
                    },
                    {
                      "featureType": "water",
                      "stylers": [
                        {
                          "color": "#e6e6e6"
                        }
                      ]
                    },
                    {
                      "featureType": "road",
                      "elementType": "labels.text",
                      "stylers": [
                        {
                          "visibility": "on"
                        }
                      ]
                    },
                    {
                      "featureType": "road",
                      "elementType": "labels.icon",
                      "stylers": [
                        {
                          "visibility": "off"
                        }
                      ]
                    },
                    {
                      "featureType": "poi",
                      "stylers": [
                        {
                          "visibility": "off"
                        }
                      ]
                    },
                    {
                      "featureType": "administrative",
                      "stylers": [
                        {
                          "visibility": "off"
                        }
                      ]
                    },
                    {
                      "featureType": "administrative.country",
                      "stylers": [
                        {
                          "visibility": "on"
                        },
                        {
                          "color": "#2c2c2c"
                        }
                      ]
                    },
                    {
                      "featureType": "administrative",
                      "elementType": "labels.text",
                      "stylers": [
                        {
                          "visibility": "off"
                        }
                      ]
                    },
                    {
                      "stylers": [
                        {
                          "saturation": -100
                        }
                      ]
                    }
                  ]
                });

    // Marker
    const marker = new google.maps.Marker({
      position: {lat: this.lat, lng: this.long},
      map: this.Map,
      icon: this.pin,
    });
  }

}


export default {

  instances: [],

  init() {

    if ( !$('.js-rp_map') .length ) { return; }

    if ( window.google && window.google.maps.Map ) {
      $('.js-rp_map').each((index, element) => {
        let _map = new Map(element);
        this.instances.push(_map);
      });
    } else {
      window.requestAnimationFrame(this.init.bind(this));
    }

  }

}
