export default {

  init() {

    const $body = $('body');

    $('.rp_modal__toggle').each((index, element) => {

      const $toggle = $(element);
      const $targetModalId = $toggle.data('target');
      const $targetModal = $(`#${$targetModalId}`);
      const $targetModalContent = $targetModal.find('.rp_modal__content');
      const $modalClose = $targetModal.find('.rp_modal__close');
      let scrollHeight,
          maxScroll,
          viewportHeight,
          visible = false;

      $toggle.on('click', e => {
        e.preventDefault();
        visible = true;
        $toggle.blur();
        $targetModal.fadeIn(400, 'swing', () => updateMetrics());
      });

      $modalClose.on('click', e => {
        e.preventDefault();
        e.stopPropagation();
        visible = false;
        $targetModal.fadeOut();
      });

      $targetModalContent.on('scroll', e => {
        if ( $targetModalContent[0].scrollTop >= maxScroll && visible ) {
          visible = false;
          slideOut();
        }
      });

      // Private Functions
      // ------------------------------------------------
      
      function updateMetrics() {
        scrollHeight = $targetModalContent[0].scrollHeight;
        viewportHeight = window.innerHeight;
        maxScroll = scrollHeight - viewportHeight;
      }

      function slideOut() {
        $targetModal.one('transitionend', () => {
          $body.removeClass('rp_featured-projects--hidden');
          $targetModal.css('transform', '').toggle(false)[0].scrollTo(0,0);
        });
        $body.addClass('rp_featured-projects--hidden');
      }

    });

  }

}
