require('colors');
const fs = require('fs-extra');
const walkSync = require('fs-walk').walkSync;
const path = require('path');
const gulp = require('gulp');
const gutil = require("gulp-util");
const watch = require('gulp-watch');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const rename = require('gulp-rename');
const plumber = require('gulp-plumber');
const express = require('express');
const webpack = require('webpack');
const imagemin = require('gulp-imagemin');
const Twig = require('twig');
const twig = Twig.twig;

const Twing = require('twing');
let loader = new Twing.TwingLoaderFilesystem(path.resolve(__dirname, 'craft/templates'));
let twing = new Twing.TwingEnvironment(loader, {debug: true});
let styleguideLoader = new Twing.TwingLoaderFilesystem(path.resolve(__dirname, 'styleguide'));
let styleguideTwing = new Twing.TwingEnvironment(styleguideLoader, {debug: true});

const browserSync = require('browser-sync');

const port = 8888;
let server;

let TEMPLATES_MAP = {};


function collectTemplates() {
  let twigTemplates = [];

  // Clear cache
  twing.loadedTemplates.clear();

  walkSync('craft/templates', function(basedir, filename, stat, next) {
      if ( !stat.isDirectory() && /\.twig$/.test(filename)) {
        // const fileContent = fs.readFileSync(`${basedir}/${filename}`).toString();
        const template = {};
        // Append basedir
        template.baseDir = basedir.replace('craft/templates/', '');
        // Append fileName
        template.fileName = filename.replace(/\.twig$/, '');
        // Append slug
        template.slug = `${basedir.replace('craft/templates/', '')}/${template.fileName}`;
        // Append data
        template.data = getTemplateData(template.slug);
        // Save template
        twigTemplates.push(template);
      }
  }, function(err) {
      if (err) console.log(err);
  });

  let groupedTemplates = groupTwigTemplates(twigTemplates);

  TEMPLATES_MAP = groupedTemplates;
}


/**
 * Groups a given list of Twig templates by directory
 * @param  {Array} twigTemplatesArray 
 * @return {Object}
 */
function groupTwigTemplates(twigTemplatesArray) {
  var grouped = {};

  for (template of twigTemplatesArray) {

    // Get parent directories
    let baseDir = template.baseDir;
    let fileName = template.fileName.replace(/(^\/|\.twig$)/g, '');

    // Ensure object tree
    let immediateParent = baseDir.split('/').reduce((parent, child) => {
      if ( !parent[child] ) { return parent[child] = {}; }
      return parent[child];
    }, grouped);

    immediateParent[fileName] = {
      twigTemplate: template,
      fileName: template.fileName,
      data: template.data,
      title: template.data.title || '',
      slug: template.slug
    };
  }

  return grouped;
}


/**
 * Given a template slug, will get respective data
 * If data file exists
 * @param  {String} templateSlug - e.g., 'pages/index' | 'components/header'
 * @return {Mixed}
 */
function getTemplateData(templateSlug) {
  const dataPath = `./craft/templates/${templateSlug}.data.js`;

  // Purge modules cache
  try {
    delete require.cache[require.resolve(dataPath)];
  } catch (err) {
    // module is not yet cached
  }

  let data = fs.existsSync(dataPath) ? require(dataPath) : {};

  return data;
}


/**
 * Styles task
 */
function stylesTask() {
  gulp.src('src/styles/main.scss')
    .pipe(plumber(function (err) {
      console.log(err.message.red, err);
    }))
    .pipe(sourcemaps.init())
    .pipe(sass({ style: 'compressed'}).on('error', sass.logError))
    .pipe(rename('styles.css'))
    .pipe(sourcemaps.write())
    .pipe(browserSync.stream())
    .pipe(gulp.dest('public/styles'));
}
gulp.task('styles', stylesTask);


/**
 * Scripts task
 */
function scriptsTask(done) {
  webpack({
    entry: `./src/scripts/app.js`,
    output: {
      path: path.resolve(__dirname, `public/scripts`),
      filename: `app.js`,
    },
    module: {
      rules: [
           {
               test: /\.js$/,
               loader: 'babel-loader',
               exclude: /node_modules/,
               query: {
                   presets: ['env']
               }
           }
       ]
    },
    stats: {
      colors: true
    },
    mode: 'development',
    devtool: 'sourcemaps',
  }, function(err, stats) {
    if(err) {
      console.log(err.message.red, err);
      process.exit();
    } else {
      gutil.log("[webpack]", stats.toString({
        // output options
      }));
      browserSync.reload();
      try {
        done();
      } catch (err) { }
    }
  });
}
gulp.task('scripts', scriptsTask);


/**
 * Fonts task
 */
function fontsTask() {
  return gulp.src('src/fonts/**/*.*')
    .pipe(gulp.dest('public/fonts'));
}
gulp.task('fonts', fontsTask);


/**
 * Image task
 */
function imagesTask() {
  return gulp.src('src/images/**/*.*')
    .pipe(imagemin())
    .pipe(gulp.dest('public/images'));
}
gulp.task('images', imagesTask);


function startExpressServer(callback) {
  try {
    const app = express();
    const templatePaths = /^\/(components|layouts|pages|partials)/;

    app.use('/styles', express.static(__dirname + '/public/styles'));
    app.use('/scripts', express.static(__dirname + '/public/scripts'));
    app.use('/images', express.static(__dirname + '/public/images'));
    app.use('/fonts', express.static(__dirname + '/public/fonts'));

    app.use('/styleguide/assets', express.static(__dirname + '/styleguide/assets'));
    app.use('/vendor', express.static(__dirname + '/styleguide/vendor'));

    // Middleware
    app.use(function (req, res, next) {
      if ( templatePaths.test(req.path) || req.path === '/' ) {
        collectTemplates();
      }
      next();
    });

    app.get('/template', (req, res) => {

      const template = req.query.template;

      if ( !template ) {
        return res.status(404).send('No template path given');
      }
      // const twigTemplate = twig({ ref: `./craft/templates/${template}.twig`});
      const twigTemplate = twing.load(`${template}.twig`);

      if ( !twigTemplate ) { return res.status(404).send('Template Not found'); }

      const data = getTemplateData(template);
      const context = data.context ? data.context : {};

      // Add dev data
      context.dev = true;

      const content = twigTemplate.render(context, (err, html) => console.log(err));
      res.send(content);
    });

    app.get('/:template*?', function (req, res) {
      let indexTemplate =  styleguideTwing.load('index.twig'); // fs.readFileSync(`styleguide/index.twig`).toString();
      let templateSlug = req.params.template ? `${req.params.template}${req.params['0']}` : '';
      let templatePath = `${templateSlug}.twig`;
      let twigTemplate = twig({ref: templatePath});
      let templateTitle = twigTemplate ? twigTemplate.data.title : templateSlug.split('/').slice(-1);
      let pageTitle = `${templateTitle} | Styleguide`;

      res.send(indexTemplate.render({ templateSlug, pageTitle, pages: TEMPLATES_MAP.pages }));
    });

    server = app.listen(port, () => {
      console.log(`Example app listening on port ${port}!`);
      if ( typeof callback === 'function' ) {
        callback();
      }
    })

  } catch (err) {
    console.log('err.message'.red, err);
  }
}

/**
 * Starts up browsersync server,
 * proxying existing fractal development server
 * @param  {String} proxyURL 
 * @return {Promise}          
 */
async function startBrowserSyncServer(proxyURL) {
  return new Promise(function (resolve, reject) {
    browserSync.init({
      proxy: proxyURL.replace('localhost','127.0.0.1')
    }, function () {
      resolve(browserSync);
    });
  });
}

gulp.task('start', ['watch'],async function () {
  startExpressServer();
  startBrowserSyncServer(`127.0.0.1:${port}`);
});


gulp.task('watch', function () {
  watch('src/styles/**/*.scss', {}, stylesTask);
  watch('src/scripts/**/*.js', {}, scriptsTask);
  watch('craft/templates/**/*.twig', {}, function () {
    // Restart server
    console.log('Restarting express server...'.green);
    if (server) {
      server.close(function () {
        startExpressServer(function () {
          browserSync.reload();
        });
      });
    } else {
      startExpressServer();
    }
  });
});

/**
 * Builds assets
 */
gulp.task('build', ['styles', 'scripts', 'fonts']);

/**
 * Builds static html files for static viewing
 */
gulp.task('static', ['build'], function () {

  // Clean dist
  fs.removeSync('./static');

  collectTemplates();

  const pages = TEMPLATES_MAP.pages;
  
  // Build pagesHTMLs
  Object.keys(pages).map( pageName => {
    const page = pages[pageName];
    const outputFile = `static/${page.fileName}/index.html`;
    const twigTemplate = twing.load(`${page.slug}.twig`);
    const data = page.data;
    const content = twigTemplate.render(data.context);
    // Write to file
    fs.outputFileSync(outputFile, content);
  });

  // Build index page
  let templateContent = fs.readFileSync(`stubs/static-index.twig`).toString();
  let indexContent = twing.render('../../stubs/static-index.twig', {pages});
  fs.outputFileSync('static/index.html', indexContent);

  // Copy assets
  fs.copySync('public', 'static');

});
