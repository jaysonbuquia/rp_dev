module.exports = {

  title: 'Projects',

  context: {
    title: 'All Projects',
    description: 'All Projects Page Template',
    projects: [
      {
        title: 'MoveOn',
        description: 'A new brand and website for the world’s largest collection of petitions',
        image: 'http://redpeak.imgix.net/uploads/images/img_16x9_moveon_posters.jpg?w=1280',
      },
      {
        title: 'Socco',
        description: 'Bringing the spirit of soccer to a new streaming platform',
      },
      {
        title: 'Brighthouse Financial',
        description: 'BCreating a brand inspired by growth',
      },
      {
        title: 'Intel',
        description: 'Building an amazing brand for Intel',
      },
      {
        title: 'Acer',
        description: 'Highlighting the people and spirit behind the brand',
      },
      {
        title: 'Coors Tek',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      },
      {
        title: 'Ollo',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      },
      {
        title: 'Lisa Sanders',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      },
      {
        title: 'Free Arts NYC',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      },
      {
        title: 'Avnet',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      },
      {
        title: 'Ollo',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      },
    ]
  }
}
