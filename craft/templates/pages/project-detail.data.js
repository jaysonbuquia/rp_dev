// Project detail data
const data = require('../components/project-detail.data.js').context;

module.exports = {

  title: 'Project Detail',

  context: {
    title: 'Project Detail',
    description: 'Project Detail Template',
    data: data,
  }
};
