let navData = require('../components/nav.data.js').context || {};
let projectData = require('../components/project-detail.data.js');

projectData = projectData.context ? projectData.context : {};

module.exports = {

  title: 'Home',

  context: {
    title: 'Home',
    description: 'All Projects Page Template',
    nav: navData.items,
    projects: [
      {
        title: 'MoveOn',
        description: 'A new brand and website for the world’s largest collection of petitions',
        project: projectData,
        image: 'http://redpeak.imgix.net/uploads/images/img_16x9_moveon_posters.jpg?w=1280',
      },
      {
        title: 'Socco',
        description: 'Bringing the spirit of soccer to a new streaming platform',
        project: projectData,
      },
      {
        title: 'Brighthouse Financial',
        description: 'Creating a brand inspired by growth',
        project: projectData,
      },
      {
        title: 'Intel',
        description: 'Building an amazing brand for Intel',
        project: projectData,
      },
      {
        title: 'Acer',
        description: 'Highlighting the people and spirit behind the brand',
        project: projectData,
      },
      {
        title: 'Coors Tek',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        project: projectData,
      },
      {
        title: 'Ollo',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        project: projectData,
      },
      {
        title: 'Lisa Sanders',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        project: projectData,
      },
      {
        title: 'Free Arts NYC',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        project: projectData,
      },
      {
        title: 'Avnet',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        project: projectData,
      },
      {
        title: 'Ollo',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        project: projectData,
      },
    ]
  }
}
