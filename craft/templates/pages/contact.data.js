const nyOffice = {
  location_name: 'New York City',
  contact_info: {
    general: `+1 212 792 8930 \nhello@red-peak.com`,
    press: `press@red-peak.com`,
    location: `RedPeak\n 625 Broadway, 2nd Floor \nNew York, NY 10012`
  },
  map_long: `-73.99644024999998`,
  map_lat: `40.726004763671774`,
  map_pin: `http://red-peak.com/images/contact/nycpin.png`,
  image: `https://marvel-live.freetls.fastly.net/canvas/2018/7/9397cb44b35f4072a87aba40c2498183?quality=80&fake=.png`,
};

const taipeiOffice = {
  location_name: 'Taipei',
  contact_info: {
    general: `+886 2 8101 6216`,
    press: `press@red-peak.com`,
    location: `RedPeak  Level 21, Suite D,\nNo. 7, Xinyi Road, Section 5\nTaipei, Taiwan 110`
  },
  map_long: `121.5626213`,
  map_lat: `25.033718`,
  map_pin: `http://red-peak.com/images/contact/taipeipin.png`,
  image: `https://cdn-std.dprcdn.net/files/acc_568777/Nc5WYA`,
}


module.exports = {

  title: 'Contact',

  context: {
    title: 'Contact',
    description: 'Contact Page Template',
    google_map_api_key: 'AIzaSyAUoh2OAueyqydEE0grogZS-DOkXvgJoUw',
    hero_text: 'We work hard to understand the people we serve, using strategy and design to create thoughtful experiences for the moments that matter. ',
    locations: [
      nyOffice,
      taipeiOffice,
    ],
    team: [
      {
        name: `Michael Birkin`,
        job_title: `Founder`,
        bio: `Michael Birkin is currently CEO of kyu, the strategic operating unit of Hakuhodo DY Holdings (HDY), and the founder of RedPeak. He began his marketing career as CEO of brand consultancy Interbrand and was a pioneer of brand valuation. After Omnicon Group acquired Interbrand in 1993, Mr. Birkin joined the holding company and quickly rose through the organization, ultimately assuming the roles of President of their largest division, Diversified Agency Services, Chairman/CEO of Omnicom Asia Pacific, and Vice-Chairman of Omnicom Group.`,
        image: `https://marvel-live.freetls.fastly.net/canvas/2018/7/a3308b1e316943ab8b7557d88d186c8c?quality=95&fake=.png`,
      },
      {
        name: `Michael Birkin`,
        job_title: `Founder`,
        bio: `Michael Birkin is currently CEO of kyu, the strategic operating unit of Hakuhodo DY Holdings (HDY), and the founder of RedPeak. He began his marketing career as CEO of brand consultancy Interbrand and was a pioneer of brand valuation. After Omnicon Group acquired Interbrand in 1993, Mr. Birkin joined the holding company and quickly rose through the organization, ultimately assuming the roles of President of their largest division, Diversified Agency Services, Chairman/CEO of Omnicom Asia Pacific, and Vice-Chairman of Omnicom Group.`,
        image: `https://marvel-live.freetls.fastly.net/canvas/2018/7/a3308b1e316943ab8b7557d88d186c8c?quality=95&fake=.png`,
      },
      {
        name: `Michael Birkin`,
        job_title: `Founder`,
        image: `https://marvel-live.freetls.fastly.net/canvas/2018/7/a3308b1e316943ab8b7557d88d186c8c?quality=95&fake=.png`,
      },
      {
        name: `Michael Birkin`,
        job_title: `Founder`,
        image: `https://marvel-live.freetls.fastly.net/canvas/2018/7/a3308b1e316943ab8b7557d88d186c8c?quality=95&fake=.png`,
      },
      {
        name: `Michael Birkin`,
        job_title: `Founder`,
        image: `https://marvel-live.freetls.fastly.net/canvas/2018/7/a3308b1e316943ab8b7557d88d186c8c?quality=95&fake=.png`,
      },
    ],
    job_openings: [
      {
        job_title: `Junior Designer`,
        job_description: `Responsible for generating conceptual and visual design solutions and have an understanding of branding and corporate identity`,
        deadline: `August 1, 2018`,
        linkedin_url: `http://www.google.com`,
      },
      {
        job_title: `Senior Designer`,
        job_description: `Responsible for generating conceptual and visual design solutions and have an understanding of branding and corporate identity`,
        deadline: `August 1, 2018`,
        linkedin_url: `http://www.google.com`,
      },
    ]
  }
};
